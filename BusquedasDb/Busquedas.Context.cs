﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BusquedasDb
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class BusquedasContainer : DbContext
    {
        public BusquedasContainer()
            : base("name=BusquedasContainer")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<DATOS_PERSONALES> DATOS_PERSONALES { get; set; }
        public virtual DbSet<ROLES_USUARIOS> ROLES_USUARIOS { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
    }
}
