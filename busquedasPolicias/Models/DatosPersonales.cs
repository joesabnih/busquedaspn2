﻿using BusquedasDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace busquedasGrl.Models
{
    public class datosPersonal
    {
        public int ID { get; set; }
        public string NOMBRES { get; set; }
        public string APELLIDOS { get; set; }
        public string TELEFONO { get; set; }
        public string EMAIL { get; set; }
        public string CONTRASENA { get; set; }
        public bool ESTATUS { get; set; }
        public DateTime FECHA_CREACION { get; set; }
        public DateTime? FECHA_ANULACION { get; set; }
        public DateTime? FECHA_VIGENCIA { get; set; }
        public int ID_ROL { get; set; }
        public string CEDULA { get; set; }

        public virtual ROLES_USUARIOS ROLES_USUARIOS { get; set; }
    }

}