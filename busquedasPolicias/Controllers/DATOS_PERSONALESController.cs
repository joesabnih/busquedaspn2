﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BusquedasDb;

namespace busquedasPolicias.Controllers
{
    public class DATOS_PERSONALESController : ApiController
    {
        private BusquedasContainer db = new BusquedasContainer();

        // GET: api/DATOS_PERSONALES
        public IQueryable<DATOS_PERSONALES> GetDATOS_PERSONALES()
        {
            return db.DATOS_PERSONALES;
        }

        // GET: api/DATOS_PERSONALES/5
        [ResponseType(typeof(DATOS_PERSONALES))]
        public IHttpActionResult GetDATOS_PERSONALES(int id)
        {
            DATOS_PERSONALES dATOS_PERSONALES = db.DATOS_PERSONALES.Find(id);
            if (dATOS_PERSONALES == null)
            {
                return NotFound();
            }

            return Ok(dATOS_PERSONALES);
        }

        // PUT: api/DATOS_PERSONALES/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDATOS_PERSONALES(int id, DATOS_PERSONALES dATOS_PERSONALES)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dATOS_PERSONALES.ID)
            {
                return BadRequest();
            }

            db.Entry(dATOS_PERSONALES).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DATOS_PERSONALESExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DATOS_PERSONALES
        [ResponseType(typeof(DATOS_PERSONALES))]
        public IHttpActionResult PostDATOS_PERSONALES(DATOS_PERSONALES dATOS_PERSONALES)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DATOS_PERSONALES.Add(dATOS_PERSONALES);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = dATOS_PERSONALES.ID }, dATOS_PERSONALES);
        }

        // DELETE: api/DATOS_PERSONALES/5
        [ResponseType(typeof(DATOS_PERSONALES))]
        public IHttpActionResult DeleteDATOS_PERSONALES(int id)
        {
            DATOS_PERSONALES dATOS_PERSONALES = db.DATOS_PERSONALES.Find(id);
            if (dATOS_PERSONALES == null)
            {
                return NotFound();
            }

            db.DATOS_PERSONALES.Remove(dATOS_PERSONALES);
            db.SaveChanges();

            return Ok(dATOS_PERSONALES);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DATOS_PERSONALESExists(int id)
        {
            return db.DATOS_PERSONALES.Count(e => e.ID == id) > 0;
        }
    }
}