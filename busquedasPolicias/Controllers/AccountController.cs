﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusquedasDb;

namespace busquedasPolicias.Controllers
{
    public class AccountController : Controller
    {
        private BusquedasContainer db = new BusquedasContainer();
        public ActionResult Login()
        {
            return PartialView("_Login");
        }

        public ActionResult Validate(DATOS_PERSONALES admin)
        {
            var _admin = db.DATOS_PERSONALES.Where(s => s.EMAIL == admin.EMAIL);
            if (_admin.Any())
            {
                if (_admin.Where(s => s.CONTRASENA == admin.CONTRASENA).Any())
                {
                    var CurrentN = _admin.SingleOrDefault(s => s.EMAIL == admin.EMAIL).NOMBRES;
                    var CurrentA = _admin.SingleOrDefault(s => s.EMAIL == admin.EMAIL).APELLIDOS;
                    TempData["CurentUser"] = CurrentN+ " " + CurrentA;
                    TempData.Keep();
                    return Json(new { status = true, message = "Inicio de sesión exitoso!" });
                }
                else
                {
                    return Json(new { status = false, message = "Contraseña invalida!" });
                }
            }
            else
            {
                return Json(new { status = false, message = "Email Invalido!" });
            }
        }
    }
}