﻿using BusquedasDb;
using busquedasGrl.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace busquedasPolicias.Helper
{
    public static class curentUser
    {
        public static datosPersonal ToModel(this DATOS_PERSONALES tbl)
        {
            var model = new datosPersonal();
            model.NOMBRES = tbl.NOMBRES;
            model.APELLIDOS = tbl.APELLIDOS;
            model.CEDULA = tbl.CEDULA;
            model.ID = tbl.ID;
            model.TELEFONO = tbl.TELEFONO;
            model.CONTRASENA = tbl.CONTRASENA;
            model.EMAIL = tbl.EMAIL;
            model.ESTATUS = tbl.ESTATUS;
            model.FECHA_ANULACION = null;
            model.FECHA_VIGENCIA = null;
            model.FECHA_CREACION = DateTime.Now;

            return model;
        }
        public static DATOS_PERSONALES ToTable(this datosPersonal model)
        {
            var tbl = new DATOS_PERSONALES();
            tbl.NOMBRES = model.NOMBRES;
            tbl.APELLIDOS = model.APELLIDOS;
            tbl.CEDULA = model.CEDULA;
            tbl.ID = model.ID;
            tbl.TELEFONO = model.TELEFONO;
            tbl.CONTRASENA = model.CONTRASENA;
            tbl.EMAIL = model.EMAIL;
            tbl.ESTATUS = model.ESTATUS;
            tbl.FECHA_ANULACION = model.FECHA_ANULACION;
            tbl.FECHA_VIGENCIA = model.FECHA_VIGENCIA;
            tbl.FECHA_CREACION = model.FECHA_CREACION;

            return tbl;
        }

      

    }
}